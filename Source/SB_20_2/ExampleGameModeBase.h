// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ExampleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SB_20_2_API AExampleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
