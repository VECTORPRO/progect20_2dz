// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_20_2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SB_20_2, "SB_20_2" );
