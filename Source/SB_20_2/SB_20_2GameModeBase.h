// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SB_20_2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SB_20_2_API ASB_20_2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
